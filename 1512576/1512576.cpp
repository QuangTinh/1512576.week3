﻿// 1512576.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512576.h"
#include<windowsx.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name5165156
WCHAR* MyCreateOpenFile(HWND hwnd);
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_1512576, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512576));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_1512576);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
HWND edit;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	int x, y;
	WCHAR* buffer = NULL;
	switch (message)
	{
		/*case (WM_SIZE):
		return HANDLE_WM_SIZE((hWnd), (wParam), (lParam), (OnSize));
		HANDLE_MSG(hWnd, WM_Size, OnSize);*/
	case WM_SIZE:
		break;
	case WM_CREATE:
	{
		GetClientRect(hWnd, &rect);
		edit = CreateWindow(L"edit", L"", WS_HSCROLL | WS_THICKFRAME | WS_VSCROLL | WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_MULTILINE | ES_AUTOVSCROLL, 0, 0, rect.right, rect.bottom, hWnd, 0, 0, 0, 0);
		break;
	}
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{

		case ID_FILE_OPEN:
		{					// Ở đây mình sẽ tạo 1 bảng để người dùng chọn đường dẫn file đến rồi đọc.
			WCHAR * chuoi = MyCreateOpenFile(hWnd);

			//DWORD dwFileSize;
			//HANDLE hFile;
			//dwFileSize = GetFileSize(hFile, NULL);
			//if (dwFileSize != 0xFFFFFFFF)
			//{
			//	LPSTR pszFileText;
			//	string utf8String = utf16ToUTF8(chuoi);
			//	LPSTR lpStr = utf8String.c_str();
			//	pszFileText = GlobalAlloc(GPTR, dwFileSize + 1);
			//	if (pszFileText != NULL)
			//	{
			//		DWORD dwRead;
			//		if (ReadFile(hFile, pszFileText, dwFileSize, &dwRead, NULL))
			//		{
			//			pszFileText[dwFileSize] = 0; // Add null terminator
			//			if (SetWindowText(hEdit, pszFileText))
			//				bSuccess = TRUE; // It worked!
			//		}
			//		GlobalFree(pszFileText);
			//	}
			//}
			//CloseHandle(hFile);

			int len = wcslen(chuoi);
			char *s = new char[len + 1];
			wcstombs(s, chuoi, len + 1);
			ifstream f;
			f.open(s);
			string line;
			string data;
			while (!f.eof()) // To get you all the lines.
			{

				getline(f, line); // Saves the line in STRING.
				data += line;
				data += "\r\n";
			}
			int a = data.length();
			WCHAR *buf = new WCHAR[a + 1];
			for (int i = 0; i < data.length(); i++)
			{
				buf[i] = data[i];
			}
			buf[a] = '\0';
			SetWindowText(edit, buf);
			f.close();
			// ghi vao edit
			break;
		}

		case ID_FILE_SAVEAS:
		{
			WCHAR* buffer1 = NULL;
			WCHAR * chuoi1 = MyCreateOpenFile(hWnd);
			int len = wcslen(chuoi1);
			char *s = new char[len + 1];
			wcstombs(s, chuoi1, len + 1);
			ifstream f;
			f.open(s);
			string line;
			string data;
			while (!f.eof()) // To get you all the lines.
			{

				getline(f, line); // Saves the line in STRING.
				data += line;
				data += "\r\n";
			}
			data.clear();

			int str = GetWindowTextLength(edit);
			buffer1 = new WCHAR[str + 1];
			GetWindowText(edit, buffer1, str + 1);
			wstring ws(buffer1);
			string strr(ws.begin(), ws.end());
			ofstream of;
			of.open(s);
			of << strr;
			of.close();


			break;
		}
		case ID_EDIT_CUT:  // khi người bấm vào nút Edit->Cut thì mình sẽ gửi 1 tin nhắn đến lớp edit rằng nó phải cut đi dòng đang tô đen đó.
			SendMessage(edit, WM_CUT, 0, 0);
			break;
		case ID_EDIT_COPY:
			SendMessage(edit, WM_COPY, 0, 0);
			break;
		case ID_EDIT_PASTE:
			SendMessage(edit, WM_PASTE, 0, 0);
			break;
		case ID_EDIT_DELETE:
			SendMessage(edit, WM_CHAR, 8, 0); //DELETE mã ASCII là 8
			break;
		case ID_FILE_NEW:
			SendMessage(edit, EM_SETSEL, 0, -1); // chon het CTRL + A
			SendMessage(edit, WM_CHAR, 8, 0);// Xoa DELETE
			break;
		case IDM_EXIT:
			PostQuitMessage(0);
			break;
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}
WCHAR szFile[260];
WCHAR* MyCreateOpenFile(HWND hwnd)
{
	OPENFILENAMEW ofn = { 0 };
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = L'\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Text Documents (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	GetOpenFileName(&ofn);
	return ofn.lpstrFile;
}

//void OnSize(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
//{
//	RECT Rect;
//	GetWindowRect(hWnd, &Rect);
//	MapWindowPoints(HWND_DESKTOP, GetParent(hWnd), (LPPOINT)&Rect, 2);
//	
//}